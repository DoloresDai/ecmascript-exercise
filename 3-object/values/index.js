export default function countTypesNumber(source) {
  return Object.values(source)
    .map(x => parseInt(x))
    .reduce((x, y) => x + y);
}
