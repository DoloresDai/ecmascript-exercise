export default function addSerialNumber(source) {
  const result = Object.assign(source);
  result.serialNumber = '12345';
  result.properties.status = 'processed';
  return result;
}
