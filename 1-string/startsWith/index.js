export default function collectCarNumberCount(collection) {
  return collection.filter(e => e.includes('粤A')).length;
}
