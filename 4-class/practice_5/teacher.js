import Person from './person';

export default class Worker extends Person {
  constructor(name, age, klass) {
    super(name, age);
    this.klass = klass;
  }

  introduce() {
    if (this.klass === undefined) {
      return `${super.introduce()} I am a Teacher. I teach no class`;
    }
    return `${super.introduce()} I am a Teacher. I teach class ${this.klass}`;
  }
}
