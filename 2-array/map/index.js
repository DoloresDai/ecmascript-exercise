export default function numberMapToWord(collection) {
  return collection.map(e => String.fromCharCode(e + 96));
}
